package xyz.lyonzy.toh.model;

import javafx.scene.Node;
import javafx.scene.layout.VBox;

/**
 * Created by Brendan Lyons on 26/02/16.
 * This class controls the moving of the blocks between towers
 */
public class Move {
    VBox sourceTower;
    VBox destinationTower;
    Boolean sourceSet;
    Boolean moved = false;

    public Move(){
        sourceSet = false;
    }

    public boolean addTower(VBox incomingTower){
        if(incomingTower.getChildren().size() !=0 || sourceTower != null){
            if(!sourceSet){
                sourceTower = incomingTower;
                sourceSet = true;
                return false;
            }
            else {
                destinationTower = incomingTower;
                move();
                return true;
            }
        }
        return true;
    }

    private void move(){
        boolean move = false;
        try{
            if(isValidMove()) move = true;
        } catch (Exception e){
            move = true;
        }

        if(move) {
            Node block = sourceTower.getChildren().get(0);
            sourceTower.getChildren().remove(0);
            destinationTower.getChildren().add(block);
            block.toBack();
            moved = true;
        }
        else moved=false;
    }


    private boolean isValidMove() throws Exception{
        return Integer.parseInt(sourceTower.getChildren().get(0).getId()) <
                Integer.parseInt(destinationTower.getChildren().get(0).getId());
    }

    public Boolean getMoved() {
        return moved;
    }
}
