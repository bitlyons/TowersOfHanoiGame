package xyz.lyonzy.toh.model;

import javafx.scene.layout.VBox;

/**
 * Created by Brendan Lyons on 26/02/16.
 * This class creates the game itself.
 */
public class Game {
    private Undo undo = new Undo();
    private int moveCount = 0;
    private int block;
    private VBox tower1;
    private VBox tower2;
    private VBox tower3;
    private VBox tower4;
    private Move move  = new Move();
    private boolean moveDone = false;
    private int game = 0;


    public Game(VBox tower1, VBox tower2, VBox tower3) {
        this.tower1 = tower1;
        this.tower2 = tower2;
        this.tower3 = tower3;
        tower4 = new VBox();
    }

    public Game(VBox tower1, VBox tower2, VBox tower3, VBox tower4) {
        this.tower1 = tower1;
        this.tower2 = tower2;
        this.tower3 = tower3;
        this.tower4 = tower4;
    }


    public boolean undoMove(){
        if (undo.undoMove(tower1, tower2, tower3, tower4)) {
           moveCount--;
           return true;
       }
        return false;
    }

    public void pushState(){
        undo.moveMade(move.sourceTower, move.destinationTower);
    }

    public void moveBlock(VBox incomingTower){
      if(moveDone) move = new Move();
        moveDone  =  move.addTower(incomingTower);
        if(move.getMoved()) {
            moveCount++;
            pushState();
        }
    }


    public void createGame(){
        for(int i = 0; i< block; i++){
            Block block = game == 2 ? new Block(30 + (i * 25), i) : new Block(50 + (i * 35), i);

            tower1.getChildren().add(block);
        }
    }



    public void setBlock(int block) {
        this.block = block;
    }

    public VBox getTower1() {
        return tower1;
    }

    public VBox getTower2() {
        return tower2;
    }

    public VBox getTower3() {
        return tower3;
    }

    public VBox getTower4() {
        return tower4;
    }

    public int getMoveCount() {
        return moveCount;
    }

    public int getBlocks(){
        return block;
    }

    public void setGameMode(int mode) {
        if (mode == 2) this.game = 2;

    }
}
