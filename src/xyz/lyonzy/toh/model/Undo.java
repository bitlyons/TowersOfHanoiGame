package xyz.lyonzy.toh.model;

import javafx.scene.layout.VBox;

import java.util.Stack;

/**
 * Created by Brendan Lyons on 24/02/16.
 * This class is used to undo any moves that where made in the game. it does this by creating a string
 * for each move and the adding it to a stack, to undo it then removes that latest item added and convert it back into
 * a two VBoxes and moves the block.
 */
public class Undo {
    Stack<String> moves = new Stack<>();
    VBox source, destination;
    String move;

    public Undo() {
    }

    public void moveMade(VBox source ,VBox destination){
        this.source=source;
        this.destination = destination;
        move = stringBuilder(source.getId()) + ">" + stringBuilder(destination.getId());
        moves.push(move);
    }

    public boolean undoMove(VBox t1, VBox t2, VBox t3, VBox t4) {
       if(moves.size() !=0) {
           move = moves.pop();
           String[] split = move.split(">");

           switch (split[0]) {
               case "1":
                   source = t1;
                   break;
               case "2":
                   source = t2;
                   break;
               case "3":
                   source = t3;
                   break;
               case "4":
                   source = t4;
           }
           switch (split[1]) {
               case "1":
                   destination = t1;
                   break;
               case "2":
                   destination = t2;
                   break;
               case "3":
                   destination = t3;
                   break;
               case "4":
                   destination = t4;
           }
           Move move = new Move();
           move.addTower(destination);
           move.addTower(source);
           return true;
       }
        return false;
    }


   private String stringBuilder(String tower){
       switch(tower){
           case "tower1" :
               return "1";
           case "tower2" :
               return "2";
           case "tower3" :
               return "3";
           case "tower4":
               return "4";
       }
       return null;
   }

}
