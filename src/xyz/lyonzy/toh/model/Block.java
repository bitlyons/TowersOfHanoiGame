package xyz.lyonzy.toh.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Created by Brendan Lyons on 26/02/16.
 * This is class creates the block used in the towers
 */
public class Block extends ImageView{

    public Block(int size, int idNumber) {
        Image image = new Image(getClass().getResource("../resources/brick.png").toExternalForm());
        this.setId(Integer.toString(idNumber)); //using Css id to compare bricks
        this.setImage(image);
        this.setFitWidth(size);
    }

}
