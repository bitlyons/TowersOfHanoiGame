package xyz.lyonzy.toh;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    static Stage stage;
    static Scene titleScreen;
    @Override
    public void start(Stage primaryStage) throws Exception{
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("view/titlescreen.fxml"));
        titleScreen = new Scene(root, 900, 420);
        primaryStage.setTitle("Towers of Hanoi");
        primaryStage.setScene(titleScreen);
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    /* The following two methods exist to make it easier to set the main window scene ]
    *  without creating a new Stage or passing the stage to other methods.*/
    public static void setPrimaryStage(Scene scene){
        stage.setScene(scene);
    }

    public static void returnToTitleScreen(){
        stage.setScene(titleScreen);
    }
    public static void main(String[] args) {
        launch(args);
    }
}
