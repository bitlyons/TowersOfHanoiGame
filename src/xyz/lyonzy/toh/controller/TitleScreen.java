package xyz.lyonzy.toh.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import xyz.lyonzy.toh.Main;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Brendan Lyons on 25/02/16.
 * it is the controller for titlescreen.fxml
 */
public class TitleScreen implements Initializable {
    @FXML
    ToggleGroup gameMode;
    @FXML
    RadioButton option1;
    @FXML
    RadioButton option2;

    @FXML
    private void playButton() {
       try {
           if (gameMode.getSelectedToggle().equals(option1)) {
               Main.setPrimaryStage(gameOne());
           }
           else  Main.setPrimaryStage(gameTwo());

           }catch (Exception e){
           //TODO create alert
           System.out.println("Failed to create Game");
           e.printStackTrace();
       }
    }

    @FXML
    private void optionsMenu() throws Exception{
        Main.setPrimaryStage(new Scene(FXMLLoader.load(getClass().getResource("../view/options.fxml")),900,420));
    }


    private Scene gameOne() throws Exception{
            return new Scene(FXMLLoader.load(getClass().getResource("../view/gameModeOne.fxml")), 900, 420);
    }


    //TODO : change to a gameTwo fxml
    private Scene gameTwo() throws Exception{
        return new Scene(FXMLLoader.load(getClass().getResource("../view/gameModeTwo.fxml")), 900, 420);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
