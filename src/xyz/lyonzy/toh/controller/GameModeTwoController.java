package xyz.lyonzy.toh.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import xyz.lyonzy.toh.Main;
import xyz.lyonzy.toh.misc.Alerts;
import xyz.lyonzy.toh.model.Game;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * Created by Brendan Lyons on 02/03/16.
 */
public class GameModeTwoController implements Initializable {

    @FXML
    VBox tower1, tower2, tower3, tower4;
    @FXML
    Button towerOneButton, towerTwoButton, towerThreeButton, towerFourButton;
    @FXML
    Text moveId;
    @FXML
    ChoiceBox brickAmount;
    Game currentGame;
    Random rand = new Random();
    int movesToUnlock;
    int countdown = 0;
    int locked = 0;
    private boolean source;
    private Button sourceButton;


    @FXML
    private void exitGame() {
        Main.returnToTitleScreen();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        newGame();
    }

    private void newGame() {
        tower1.getChildren().clear();
        tower2.getChildren().clear();
        tower3.getChildren().clear();
        tower4.getChildren().clear();
        towerOneButton.setDisable(false);
        towerTwoButton.setDisable(false);
        towerThreeButton.setDisable(false);
        towerFourButton.setDisable(true);

        moveId.setText("Moves : 0");
        currentGame = new Game(tower1, tower2, tower3, tower4);
        currentGame.setGameMode(2);
        currentGame.setBlock(Integer.parseInt(brickAmount.getValue().toString()));
        currentGame.createGame();


        movesToUnlock = rand.nextInt(10);

    }

    @FXML
    private void towerOneButton() {
        if (!source && tower1.getChildren().size() < 1) {
        } else {
            moveBlock(currentGame.getTower1());
            if (source) sourceButtonEnable();
            else {
                setSourceButton(towerOneButton);
                sourceButtonDisable();
            }
        }
    }

    @FXML
    private void towerTwoButton() {
        if (!source && tower2.getChildren().size() < 1) {
        } else {
            moveBlock(currentGame.getTower2());
            if (source) sourceButtonEnable();
            else {
                setSourceButton(towerTwoButton);
                sourceButtonDisable();
            }
        }
    }

    @FXML
    private void towerThreeButton() {
        if (!source && tower3.getChildren().size() < 1) {
        } else {
            moveBlock(currentGame.getTower3());
            if (source) sourceButtonEnable();
            else {
                setSourceButton(towerThreeButton);
                sourceButtonDisable();
            }
        }
    }

    @FXML
    private void towerFourButton() {
        if (!source && tower4.getChildren().size() < 1) {
        } else {
            moveBlock(currentGame.getTower4());
            if (source) sourceButtonEnable();
            else {
                setSourceButton(towerFourButton);
                sourceButtonDisable();
            }
        }
    }


    private void setSourceButton(Button sourceButton) {
        this.sourceButton = sourceButton;
        sourceButton.setDisable(true);
    }

    private void sourceButtonDisable() {
        source = true;
        sourceButton.setDisable(true);
    }

    private void sourceButtonEnable() {
        source = false;
        sourceButton.setDisable(false);
    }

    @FXML
    private void moveBlock(VBox tower) {
        currentGame.moveBlock(tower);
        moveId.setText("Moves : " + currentGame.getMoveCount());
        if (tower2.getChildren().size() == currentGame.getBlocks()) gameWon();


        if (locked == movesToUnlock) {
            towerFourButton.setDisable(false);
            movesToUnlock = rand.nextInt(10);
            countdown = rand.nextInt(10);
            locked = 0;

        } else if (countdown < -1) {
            towerFourButton.setDisable(true);

        } else {
            countdown--;
        }
        locked++;

    }

    @FXML
    private void undoMove() {
        currentGame.undoMove();
        moveId.setText("Moves : " + currentGame.getMoveCount());
    }


    @FXML
    private void resetGame() {
        if (Alerts.resetGame()) {
            newGame();
        }
    }

    @FXML
    private void changeBricks() {
        newGame();
    }


    @FXML
    private void tower1move() {
        moveBlock(tower1);
    }

    private void gameWon() {
        if (Alerts.win(currentGame.getMoveCount())) {
            newGame();
        } else returnToTitleScreen();
    }

    @FXML
    public void returnToTitleScreen() {
        Main.returnToTitleScreen();
    }


    private void mouse() {
        newGame();
        mouse();
    }


}