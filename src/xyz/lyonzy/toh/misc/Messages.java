package xyz.lyonzy.toh.misc;

/**
 * Created by lyonzy on 28/02/16.
 */
public class Messages {


    public static String textBox(int message) {
        switch(message){
            case 0 :
                return "Game Reset!";
            case 1 :
                return "You Cannot Make That Move!";
        }
        return null;
    }
}
