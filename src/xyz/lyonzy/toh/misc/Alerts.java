package xyz.lyonzy.toh.misc;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import java.util.Optional;

/**
 * Created by Brendan Lyons on 27/02/16.
 * it contains all the alert boxes used through out the game
 */
public class Alerts {

    public static boolean win(int moves){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Congratulations : you have won the game! \n It took you a total of "+ moves);
        alert.setContentText("Would you like to play again?");
        alert.setTitle("You Won");

        Optional<ButtonType> playAgain = alert.showAndWait();
        return playAgain.get() == ButtonType.OK;
    }

    public static boolean resetGame(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("You are about to reset the game!");
        alert.setContentText("Are you sure you wish to do that?");
        alert.setTitle("Reset Game");

        Optional<ButtonType> reset = alert.showAndWait();
        return reset.get() == ButtonType.OK;
    }
}
